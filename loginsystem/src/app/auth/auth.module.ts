import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './about/about.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import {  FormsModule, ReactiveFormsModule, } from '@angular/forms';
import { HttpClientModule,} from '@angular/common/http';



@NgModule({
  declarations: [
    AboutComponent,
    LoginComponent,
    RegistrationComponent,

 ],

  imports: [CommonModule , ReactiveFormsModule,FormsModule, HttpClientModule, ],
  exports: [AboutComponent, LoginComponent, RegistrationComponent,],
  providers:[]
})
export class AuthModule { }

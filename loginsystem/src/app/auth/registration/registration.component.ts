import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {  FormBuilder,FormGroup,Validators,} from "@angular/forms";
import { Router } from '@angular/router';
import { CountriesService } from "src/app/countries.service";
import { Country } from 'src/app/country';
import { CustomValidatorsService } from 'src/app/custom-validators.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit 
{
  RegistrationForm: FormGroup | any = null;
  genders = ["Male", "Female"];
  countries: Country[] = []

  constructor(private countriesService: CountriesService,
               private formBuilder: FormBuilder,
               private CustomValidatorsService: CustomValidatorsService,
               private http : HttpClient,
               private router :Router,
              )
  {

}

  ngOnInit()
  {
    this.countries = this.countriesService.getCountries();

    this.RegistrationForm = this.formBuilder.group({
      personName: this.formBuilder.group({
        firstName: ['', [Validators.required, Validators.minLength(2)]],
        lastName: ['', [Validators.required, Validators.minLength(2)]],
      }),
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]] ,
      mobile: ['', [Validators.required, Validators.pattern(/^[789]\d{9}$/)]],
      dateOfBirth: ['', [Validators.required ,this.CustomValidatorsService.minimumAgeValidator(18)]],
      gender: ['', [Validators.required]],
      countryID: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      confirmPassword: ['', [Validators.required]],
    }, {
      validators: [
        this.CustomValidatorsService.compareValidator("confirmPassword", "password")
      ]
    });

}

registration(){
    this.http.post<any>("http://localhost:3000/registerUsers",this.RegistrationForm.value)
    .subscribe(res=>{     
      alert("registration successfull");
      this.RegistrationForm.reset();
      this.router.navigate(['login']);
    },err=>{
    alert("something went wrong")
    })
  }

  
}















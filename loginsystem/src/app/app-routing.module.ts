import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationComponent } from './auth/registration/registration.component';
import { LoginComponent } from './auth/login/login.component';
import { AboutComponent } from './auth/about/about.component';
import { AuthGuard  } from 'src/app/auth/auth.guard';

const routes: Routes = [
  {path: '',redirectTo:'login',pathMatch:'full'},
  {path:'registration',component:RegistrationComponent},
  {path:'login',component:LoginComponent},
  {path:'about',canActivate: [AuthGuard], component:AboutComponent},
 

];

@NgModule({
  imports: [RouterModule.forRoot(routes),BrowserModule],
  exports: [RouterModule],
  providers:[AuthGuard]

})
export class AppRoutingModule { }





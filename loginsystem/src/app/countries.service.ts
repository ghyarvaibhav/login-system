import { Injectable } from '@angular/core';
import {Country} from 'src/app/country'

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  constructor()
   {   
  }
  getCountries():Country[]
  {
    return[
      new Country(1,"Indian")
    ];
  }
}
